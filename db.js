const mysql = require('mysql');
const util = require('util');
require('dotenv').config();
var con = mysql.createPool({
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
    charset: process.env.charset
});
const query = util.promisify(con.query).bind(con);
module.exports = {
    async get(key) {
        let results = await query(`SELECT valore FROM daemonlisa where chiave = ?`, [key]);
        return results != null && results.length > 0 ? results[0].valore : null;
    },
    async set(key, value) {
        let res = await this.get(key);
        if (res != null) query(`UPDATE daemonlisa SET valore = ? where chiave = ?`, [value, key]);
        else await query(`INSERT INTO daemonlisa VALUES(?, ?)`, [key, value]);
    },
    async delete(key) {
        await query(`DELETE FROM daemonlisa where chiave = ?`, [key]);
    },
    async getAllUserIds() {
        return await query(`select d.chiave from daemonlisa d WHERE d.chiave not like '%\\_%' and d.chiave not like '%\\-%'`);
    }

}