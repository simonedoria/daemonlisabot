const moment = require('moment-timezone');
const puppeteer = require('puppeteer');
const db = require('./db');
module.exports = {
    printTasks(attivita) {
        let returnStm = "";
        for (let i = 0; i < attivita.length; i++) {
            let att = attivita[i];
            returnStm += i > 0 ? '\n' : '';
            returnStm += `<b>Progetto</b>: ${att.pr.progetto.name}\n`;
            returnStm += `<b>Attività</b>: ${att.pr.macroAttivita.filter(m => m.id == att.at)[0].name}\n`;
            returnStm += '<b>Tasks</b>:\n'
            for (let z = 0; z < att.tx.length; z++) {
                let t = att.tx[z];
                returnStm += `###### ${z} - ${t.or}h #####\n`;
                returnStm += `${t.tx}\n`;
                returnStm += '################\n\n';
            }
        }
        returnStm += 'Quando vuoi, /rendiconta !\nOppure puoi rimuovere un task lanciando il comando /remove !';
        return returnStm;
    },
    addslashes(string) {
        return string.replace(/\\/g, '\\\\').
            replace(/\u0008/g, '\\b').
            replace(/\t/g, '\\t').
            replace(/\n/g, '\\n').
            replace(/\f/g, '\\f').
            replace(/\r/g, '\\r').
            replace(/'/g, '\\\'').
            replace(/"/g, '\\"');
    },
    async aggiornaProgetti(id) {
        let credenziali = await db.get(id);
        credenziali = credenziali.split(" ");
        let user = credenziali[0];
        let pass = credenziali[1];
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox']
        });
        const page = await browser.newPage();
        await page.goto('https://crm.sinapsys.it/index.php?module=users/login', { waitUntil: 'networkidle2' });

        await page.waitForSelector('input[name=username]');
        await page.waitForSelector('input[name=password]');
        await page.waitForSelector('button[type=submit]');

        await page.$eval('input[name=username]', (el, value) => el.value = value, user);
        await page.$eval('input[name=password]', (el, value) => el.value = value, pass);
        await page.click('button[type="submit"]');
        try {
            await page.waitForSelector('.page-title', { timeout: 5000 });
        } catch (e) {
            await browser.close();
            return { status: "error", message: "Credenziali errate" };
        }
        // logged
        await page.goto('https://crm.sinapsys.it/index.php?module=items/items&path=41', { waitUntil: 'networkidle2' });
        let btnAddSelector = 'body > div.page-container > div.page-content-wrapper > div > div > div.row > div > div:nth-child(7) > div:nth-child(1) > div > button';
        await page.waitForSelector(btnAddSelector);
        await page.click(btnAddSelector);

        try {
            await page.waitForSelector('#fields_403', { timeout: 5000 });
        } catch (e) {
            await browser.close();
            return { status: "error", message: "Errore inaspettato" };
        }
        await page.waitForTimeout(1000);
        await page.click('#fields_404_chosen');
        await page.waitForTimeout(1000);
        let progetti = await page.evaluate(() => Array.from(document.querySelectorAll('#fields_404_chosen > div > ul > li'), e => e.textContent));
        let obj = [];
        for (let i = 1; i < progetti.length; i++) {
            if (i != 1) await page.click('#fields_404_chosen');
            await page.waitForTimeout(500);
            // clicco l'unico elemento risultante
            await page.click(`#fields_404_chosen > div > ul > li:nth-child(${i + 1})`);
            await page.waitForTimeout(500);
            // prendo la corrispettiva sottolista di macroattività
            await page.click('#select2-fields_405-container');
            await page.waitForTimeout(500);
            let macroAttivita = await page.evaluate(() => Array.from(document.querySelectorAll('#select2-fields_405-results > li'), e => e.textContent));
            macroAttivita = macroAttivita.map((el, index) => {
                return {
                    id: index + 1,
                    name: el
                }
            });
            await page.waitForTimeout(500);
            obj.push({
                progetto: {
                    id: i + 1,
                    name: progetti[i]
                },
                macroAttivita: macroAttivita
            })
        }
        await browser.close();
        db.set(`${id}_progetti`, JSON.stringify(obj));
        return { status: "OK", data: obj };
    },
    async inject_jquery(page) {
        await page.evaluate(() => {
            var jq = document.createElement("script")
            jq.src = "https://code.jquery.com/jquery-3.2.1.min.js"
            document.querySelector("head").appendChild(jq)
        })
        const watchDog = page.waitForFunction('window.jQuery !== undefined');
        await watchDog;
    },
    async rendiconta(id, progetto, macroattivita, ore, from, descrizione) {
        let credenziali = await db.get(id);
        credenziali = credenziali.split(" ");
        let user = credenziali[0];
        let pass = credenziali[1];
        let data = moment().format('YYYY-MM-DD');
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox']
        });
        const page = await browser.newPage();
        await page.goto('https://crm.sinapsys.it/index.php?module=users/login', { waitUntil: 'networkidle2' });

        await page.waitForSelector('input[name=username]');
        await page.waitForSelector('input[name=password]');
        await page.waitForSelector('button[type=submit]');

        await page.$eval('input[name=username]', (el, value) => el.value = value, user);
        await page.$eval('input[name=password]', (el, value) => el.value = value, pass);
        await page.click('button[type="submit"]');
        try {
            await page.waitForSelector('.page-title', { timeout: 5000 });
        } catch (e) {
            await browser.close();
            return { status: "error", message: "Credenziali errate" };
        }
        // logged
        await page.goto('https://crm.sinapsys.it/index.php?module=items/items&path=41', { waitUntil: 'networkidle2' });
        let btnAddSelector = 'body > div.page-container > div.page-content-wrapper > div > div > div.row > div > div:nth-child(7) > div:nth-child(1) > div > button';
        await page.waitForSelector(btnAddSelector);
        await page.click(btnAddSelector);

        try {
            await page.waitForSelector('#fields_403', { timeout: 5000 });
        } catch (e) {
            await browser.close();
            return { status: "error", message: "Errore inaspettato" };
        }

        //data
        await page.$eval('#fields_403', (el, value) => el.value = value, data);
        //progetto
        await page.waitForTimeout(1000);
        await page.click('#fields_404_chosen');
        await page.waitForTimeout(1500);
        await page.click(`#fields_404_chosen > div > ul > li:nth-child(${progetto})`);
        //macroattivita
        await page.waitForTimeout(1500);
        await page.click('#select2-fields_405-container');
        await page.waitForTimeout(2000);
        for (let i = 1; i < parseInt(macroattivita); i++) {
            page.keyboard.press('ArrowDown');
            await page.waitForTimeout(100);
        }
        page.keyboard.press('Enter');
        await page.waitForTimeout(500);
        //descrizione
        await page.$eval('#fields_406', (el, value) => el.value = value, descrizione);
        //ore
        await page.$eval('#fields_407', (el, value) => el.value = value, ore);
        switch (from) {
            case "1":
                from = "85";
                break;
            case "2":
                from = "83";
                break;
            case "3":
                from = "84";
                break;
        }
        let selectorFrom = `#uniform-fields_433_${from}`;
        await page.waitForSelector(selectorFrom);
        await page.click(selectorFrom);

        let btnSaveSelector = '#items_form > div.modal-footer > button.btn.btn-primary.btn-primary-modal-action';
        await page.waitForSelector(btnSaveSelector);
        await page.click(btnSaveSelector);
        await browser.close();
        return { status: "OK", message: "Rendicontazione inserita con successo!" };
    }
}