const { Telegraf } = require('telegraf');
const core = require('./core');
const db = require('./db');
var CronJob = require('cron').CronJob;
require('dotenv').config();

const bot = new Telegraf(process.env.t_token);

bot.start((ctx) => {
  ctx.reply("Grazie di aver capito che fare la rendicontazione è importante, per cominciare, inviami le tue credenziali di accesso al CRM, tranquillo terrò il segreto ;). \nPer farlo scrivi /credenziali USERNAME PASSWORD");
});

bot.help((ctx) => {
  ctx.reply(`- /rendiconta - Avvia il processo di rendicontazione guidata\n
  - /aggiorna - Aggiorna la lista delle tue attività\n
  - /credenziali - Aggiorna le tue credenziali (shh! non dirle a nessuno!)\n
  - /tasks - Vedi la lista corrente dei tuoi tasks aggiunti\n
  - /add - Aggiungi un task che hai effettuato [ES: /add fix bug]\n
  - /remove - Rimuovi un task dalla lista\n
  - /removeall - Cancella tutti i task in corso`);
});

bot.command('aggiorna', async (ctx) => {
  try {
    let id = ctx.from.id;
    let progetti = await core.aggiornaProgetti(id);
    if (progetti.status == 'error') ctx.reply(`MMM....qui gatta ci cova...non sono in grado di vedere le tue attività...ecco il motivo: ${progetti.message}`);
    else {
      await db.set(`${id}-progetti`, JSON.stringify(progetti.data));
      ctx.reply(`Ho aggiornato le tue attività.\nOra però rendiconta se non lo hai ancora fatto!\nMi raccomando...la vera Lisa non è così clemente come me!`);
    }
  } catch (ex) {
    ctx.reply("Si è verificato un problema, riprova fra poco...")
  }

});

bot.command('tasks', async (ctx) => {
  try {
    let id = ctx.from.id;
    let attivita = await db.get(`${id}_att`);
    if (attivita == null) {
      ctx.reply("Non hai ancora inserito nessun task, per farlo, utilizza il comando /add DESCRIZIONE TASK");
      return;
    }
    try {
      attivita = JSON.parse(attivita);
      if (attivita.length > 0 && attivita[0].pr == null) {
        ctx.reply("Non hai ancora inserito nessun task, per farlo, utilizza il comando /add DESCRIZIONE TASK");
        return;
      }
    } catch(ex) {
        ctx.reply("Non hai ancora inserito nessun task, per farlo, utilizza il comando /add DESCRIZIONE TASK");
        return;
    }
    await ctx.replyWithHTML(core.printTasks(attivita));
  } catch (ex) {
    ctx.reply("Si è verificato un problema, riprova fra poco...")
  }

});

bot.command('add', async (ctx) => {
  try {
    let id = ctx.from.id;
    let att = ctx.message.text;
    att = att.replace("/add", "").trim();
    if (att == '') {
      ctx.reply("Esegui il comando in questo modo:\n\n/add DESCRIZIONE DEL TASK\n\nIn un unico messaggio!");
      return;
    }
    ctx.reply('Complimenti per aver completato il task!\nOra dimmi di che progetto si tratta e lo ricorderò per te. ;)');

    // FETCH DEI PROGETTI 
    let progetti = await db.get(`${id}_progetti`);
    if (progetti == null) {
      ctx.reply('MMM..non hai ancora scaricato i progetti con il comando /aggiorna\nLo farò io per te! (Questa volta :@)');
      await core.aggiornaProgetti(id);
    }
    let attivita = await db.get(`${id}_att`);
    if (attivita != null) {
      attivita = JSON.parse(attivita);
      attivita = attivita.map(a => a.tx);
      let found = false;
      for (let i = 0; i < attivita.length; i++) {
        if (attivita[i] === att) {
          found = true;
          break;
        }
      }
      if (found) {
        att += '_';
      }
    }
    // SALVO TEMPORANEAMENTE SU DB IL TASK APPENA INSERITO, IN MODO DA POTERLO RICHIAMARE NELLA FUNZIONE DI CALLBACK
    await db.set(`${id}_taskcorrente`, att.trim());
    progetti = JSON.parse(progetti);
    let inlineKeyboards = progetti.map(pr => {
      return {
        text: pr.progetto.name,
        callback_data: `pr_${pr.progetto.id}`
      }
    }).map(el => [el]);
    ctx.telegram.sendMessage(id, 'Scegli il progetto:', {
      reply_markup: {
        inline_keyboard: inlineKeyboards
      }
    });
  } catch (ex) {
    ctx.reply('Si è verificato un problema :(, riprova in seguito.');
    //ctx.reply(ex.message);
  }

});

bot.command('remove', async (ctx) => {
  try {
    let id = ctx.from.id;
    let attivita = await db.get(`${id}_att`);
    if (attivita == null) {
      ctx.reply('Non hai ancora inserito ancora nessuna attività, puoi aggiungere un task durante il giorno subito dopo averlo ultimato lanciando il comando /add DESCRIZIONE TASK\nEs: /add oggi mi sono decantato')
      return;
    }
    attivita = JSON.parse(attivita);
    attivita = attivita.map(a => a.tx);
    let tasks = [];
    for (let i = 0; i < attivita.length; i++) {
      for (let z = 0; z < attivita[i].length; z++) {
        tasks.push(attivita[i][z].tx);
      }
    }
    let inlineKeyboards = tasks.map((at, index) => {
      return {
        text: at,
        callback_data: `rm_${index}`
      }
    }).map(el => [el]);
    ctx.telegram.sendMessage(ctx.chat.id, 'Seleziona l\attività da rimuovere', {
      reply_markup: {
        inline_keyboard: inlineKeyboards
      }
    });
  } catch (ex) {
    console.log(ex.message);
    ctx.reply('Si è verificato un problema, riprova fra poco...')
  }
});

bot.command('removeall', async (ctx) => {
  try {
    let id = ctx.from.id;
    await db.delete(`${id}_att`);
    ctx.reply('Lista dei task svuotata con successo!');
  } catch (ex) {
    ctx.reply('Si è verificato un problema, riprova fra poco...');
  }
});

bot.command('credenziali', async (ctx) => {
  try {
    let credenziali = ctx.message.text;
    credenziali = credenziali.replace("/credenziali ", "");
    let credenzialiArr = credenziali.split(" ");
    let id = ctx.from.id;
    if (credenzialiArr.length == 2) {
      await db.set(id, credenziali);
      ctx.reply(`Ho memorizzato le tue credenziali, puoi cambiarle in ogni momento rilanciando questo comando!`);
      ctx.reply('Vediamo un po\' quali sono i tuoi progetti..attendi qualche secondo..');
      let progetti = await core.aggiornaProgetti(id);
      if (progetti.status == 'error') ctx.reply(`MMM....qui gatta ci cova...non sono in grado di vedere le tue attività...ecco il motivo: ${progetti.message}`);
      else {
        await db.set(`${id}_progetti`, JSON.stringify(progetti.data));
        ctx.reply(`Perfetto, ora so tutto e sono pronta a partire.\nD'ora in poi, lancia il comando /rendiconta e ti seguirò passo dopo passo nella tua rendicontazione giornaliera!\nBuon lavoro!`);
        ctx.reply(`PS: se il tuo BOSS ha aggiunto nuove attività per te, lancia il comando /aggiorna per permettermi di conoscerle!`);
      }
    } else {
      ctx.reply("Inserisci correttamente le credenziali: USERNAME [UN SOLO SPAZIO] PASSWORD\nDai, puoi farcela!");
    }
  } catch (ex) {
    ctx.reply("Si è verificato un problema, riprova fra poco...")
  }

});

bot.command('rendiconta', async (ctx) => {
  try {
    ctx.deleteMessage();
    let id = ctx.from.id;
    let credenziali = await db.get(id);
    if (credenziali == null) {
      ctx.reply('MMM...non mi hai ancora detto quali sono le tue credenziali. Per poter iniziare a rendicontare lancia il comando /credenziali USERNAME PASSWORD');
      return;
    } 
      ctx.reply('Dunque, rendicontiamo le tue attività di oggi...');
    let attivita = await db.get(`${id}_att`);
      if (attivita == null) {
        ctx.replyWithMarkdown('Prima di rendicontare aggiungi almeno un task con il comando */ADD task eseguito*');
        return;
      } 
      let inlineKeyboards = [];
      inlineKeyboards.push([{ text: 'UFFICIO', callback_data: `fr_a_1` }]);
      inlineKeyboards.push([{ text: 'CASA', callback_data: `fr_a_2` }]);
      inlineKeyboards.push([{ text: 'TRASFERTA', callback_data: `fr_a_3` }]);
      ctx.telegram.sendMessage(ctx.chat.id, 'Dove hai svolto la tua attività?', {
        reply_markup: {
          inline_keyboard: inlineKeyboards
        }
      });
    
  } catch (ex) {
    ctx.reply("Si è verificato un problema, riprova fra poco...")
  }

});

bot.on('callback_query', async (ctx) => {
  try {
    ctx.deleteMessage();
    let q = ctx.callbackQuery.data;
    let qArr = q.split('_');
    let id = ctx.from.id;
    let command = qArr[0];
    let key = qArr[1];
    let inlineKeyboards = [];
    let keyAtt = null;
    let pr = null;
    let progetti = await db.get(`${id}_progetti`);
    if (progetti == null) {
      ctx.reply("Mi serve un attimo per aggiornare i progetti...");
      await core.aggiornaProgetti(id);
    }
    progetti = JSON.parse(progetti);
    switch (command) {
      case 'pr':
        pr = progetti.filter(pr => pr.progetto.id == key)[0];
        inlineKeyboards = pr.macroAttivita.map(at => {
          return {
            text: at.name,
            callback_data: `at_${pr.progetto.id}_${at.id}`
          }
        }).map(el => [el]);
        ctx.telegram.sendMessage(ctx.chat.id, 'Che attività hai svolto?', {
          reply_markup: {
            inline_keyboard: inlineKeyboards
          }
        });
        break;
      case 'at':
        pr = progetti.filter(pr => pr.progetto.id == key)[0];
        keyAtt = qArr[2];
        inlineKeyboards = [];
        for (let i = 1; i <= 8; i++) {
          inlineKeyboards.push([{ text: i, callback_data: `or_${pr.progetto.id}_${keyAtt}_${i}` }]);
        }
        ctx.telegram.sendMessage(ctx.chat.id, 'Quanto tempo hai dedicato?', {
          reply_markup: {
            inline_keyboard: inlineKeyboards
          }
        });
        break;
      case 'or':
        pr = progetti.filter(pr => pr.progetto.id == key)[0];
        keyAtt = qArr[2];
        let keyOr = qArr[3];
        let currentAtt = await db.get(`${id}_taskcorrente`);
        if (currentAtt != null) {
          let attivita = await db.get(`${id}_att`);
          if (attivita != null) {
            attivita = JSON.parse(attivita);
            if (attivita.length > 0 && attivita[0].pr == null) {
              attivita = [];
            }
          } else {
            attivita = [];
          }
          let taskToFind = attivita.filter(at => at.pr.progetto.id == parseInt(key) && at.at == parseInt(keyAtt));
          let toAdd = { tx: currentAtt, or: keyOr};
          if (taskToFind.length > 0) {
            let indexOf = attivita.indexOf(taskToFind[0]);
            if (indexOf != -1) {
              attivita[indexOf].tx.push(toAdd);
            } else {
              attivita.push({
                pr: pr,
                at: keyAtt,
                tx: [toAdd]
              });
            }
          } else {
            attivita.push({
              pr: pr,
              at: keyAtt,
              tx: [toAdd]
            });
          }
          await db.set(`${id}_att`, JSON.stringify(attivita));
          ctx.replyWithHTML(core.printTasks(attivita));
        } else {
          ctx.reply("Qualcosa è andato storto, non trovo più il tuo task :(\nPuoi provare a reinserirlo? Grazie :*");
        }
        break;
      case 'fr':
        let attivita = await db.get(`${id}_att`);
        if (attivita == null) {
          ctx.reply("Non hai ancora inserito ancora nessuna attività, puoi aggiungere un task durante il giorno subito dopo averlo ultimato lanciando il comando /add DESCRIZIONE TASK\nEs: /add oggi mi sono decantato")
          return;
        }
        let from = qArr[2];
        attivita = JSON.parse(attivita);
        for (let i = 0; i < attivita.length; i++) {
          let att = attivita[i];
          ctx.reply(`Sto inserendo la rendicontazione....attendi....`);
          let sumOre = 0;
          let tasks = "";
          for (let z = 0; z < att.tx.length; z++) {
            sumOre += parseInt(att.tx[z].or);
            tasks += `${att.tx[z].tx}`;
            tasks += z < att.tx.length - 1 ? '\n' : '';
          }
          let res = await core.rendiconta(id, att.pr.progetto.id, att.at, sumOre, from, tasks);
          if (res.status == 'error') {
            ctx.replyWithMarkdown(`Errore nell'inserimento della rendicontazione per il progetto *${att.pr.progetto.name}*\nErrore: ${res.message}`);
          } else {
            ctx.replyWithMarkdown(`Rendicontazione per il progetto *${att.pr.progetto.name}* inserita con successo!`);
          }
        }
        await db.delete(`${id}_att`);
        ctx.reply(`Processo di rendicontazione giornaliera completato! Ci vediamo domani. ;)`);
        break;
      case 'rm':
        let attivitaa = await db.get(`${id}_att`);
        attivitaa = JSON.parse(attivitaa);
        let prId = null;
        let attId = null;
        let taskId = null;
        let countIndex = 0;
        let found = false;
        let projectEmpty = false;
        // identifico gli id da eliminare
        for (let i = 0; i < attivitaa.length; i++) {
          for (let z = 0; z < attivitaa[i].tx.length; z++) {
            if (countIndex === parseInt(key)) {
              prId = attivitaa[i].pr.progetto.id;
              taskId = z;
              attId = parseInt(attivitaa[i].at);
              found = true;
              break;
            }
            countIndex++;
          }
          if (found) break;
        }
        // rimuovo dal progetto il task
        for (let i = 0; i < attivitaa.length; i++) {
          if (attivitaa[i].pr.progetto.id === prId && attivitaa[i].at == attId) {
            attivitaa[i].tx.splice(taskId, 1);
            if (attivitaa[i].tx.length == 0) {
              projectEmpty = true;
            }
            break;
          }
        }
        if (projectEmpty) {
          attivitaa = attivitaa.filter(a => a.pr.progetto.id !== prId);
        }        
        let returnStm = core.printTasks(attivitaa);
        if (attivitaa.length > 0) {
          await db.set(`${id}_att`, JSON.stringify(attivitaa));
          ctx.replyWithHTML(`Task rimosso, ecco la lista dei tuoi task\n\n${returnStm}`);
        } else {
          await db.delete(`${id}_att`);
          ctx.reply(`Non hai nessun task in coda, la rendicontazione non verrà eseguita.`);
        }
        break;
      }
  } catch (ex) {
    ctx.reply("Si è verificato un problema, riprova fra poco...")
  }

})

bot.launch();

var job = new CronJob('0 0 18 * * 1-5', async function () {
  let userIds = await db.getAllUserIds();
  console.log(userIds);
  //userIds = JSON.parse(userIds);
  if (userIds.length > 0) {
    for (let i = 0; i < userIds.length; i++) {
      bot.telegram.sendMessage(userIds[i].chiave, "Ciao! La giornata lavorativa è finita ed è arrivato il momento di rendicontare.\nLancia il comando /rendiconta e segui la procedura guidata.\nFallo, è importante!");
    }
  }
}, null, true, 'Europe/Rome');

job.start();