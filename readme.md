<div id="top"></div>

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]

<br />
<div align="center">
  <a href="https://dev.azure.com/sinapsyssrl/DaemonLisaBot">
    <img src="images/daemonlisa.jpg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">DaemonLisaBot</h3>

  <p align="center">
    Il bot di rendicontazione di Sinapsys Srl
    <br />
    <a href="https://telegram.me/DaemonLisaBot"><strong>Link al Bot >>>LIVE<<< »</strong></a>
    <br />
    <br />
    <a href="https://dev.azure.com/sinapsyssrl/DaemonLisaBot/issues">Report Bug</a>
    ·
    <a href="https://dev.azure.com/sinapsyssrl/DaemonLisaBot/issues">Richiedi nuove features</a>
  </p>
</div>

## Getting Started

Di seguito i passaggi per poter utilizzare il bot:

### Linee guida di utilizzo

1. Scaricare telegram dallo store del vostro smartphone / tablet
2. Andare al seguente link --> [https://telegram.me/DaemonLisaBot] ed entrare nel bot
3. Per la prima volta dovrete fornire le vostre credenziali di accesso al crm utilizzando questo comando (con comando si intende un messaggio che inviamo al bot):
   ```js
   /credenziali username password
   ```
3. Per aggiungere un task appena effettuato durante la giornata lavorativa, basta inviare un messaggio al bot di questo tipo:
   ```js
   /add fix bug di allineamento tracciato sulla piattaforma ausiliaria 
   ```
4. Infine basta lanciare il comando:
   ```js
   /rendiconta
   ```
   e partirà la procedura guidata di rendicontazione, riferita al giorno corrente.

5. Per consultare tutti i comandi disponibili, invia il messaggio:
   ```js
   /help
   ```

Ogni contributo è ben accetto!

<p align="right">(<a href="#top">torna sù</a>)</p>